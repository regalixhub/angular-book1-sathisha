import { TestBed } from '@angular/core/testing';

import { BookAppService } from './book-app.service';

describe('BookAppService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BookAppService = TestBed.get(BookAppService);
    expect(service).toBeTruthy();
  });
});
