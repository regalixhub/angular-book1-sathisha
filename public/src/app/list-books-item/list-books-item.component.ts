import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BookAppService } from '../book-app.service';

@Component({
  selector: 'app-list-books-item',
  templateUrl: './list-books-item.component.html',
  styleUrls: ['./list-books-item.component.css']
})
export class ListBooksItemComponent implements OnInit {

  @Input() public booksData;
  @Output() public childEvent = new EventEmitter();

  constructor(private bookService: BookAppService,
              private _router: Router,
              private _route: ActivatedRoute) { }

  ngOnInit() {
  }
  edit(index) {
    // this.childEvent.emit(index);
    this._router.navigate(['/edit', index]);
  }
  delete(i) {
    this.bookService.deleteBook(i);
  }
  showDetails(index) {
    this._router.navigate(['/showDetails', index]);
  }

}
