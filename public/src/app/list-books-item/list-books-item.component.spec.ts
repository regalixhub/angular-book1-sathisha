import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListBooksItemComponent } from './list-books-item.component';

describe('ListBooksItemComponent', () => {
  let component: ListBooksItemComponent;
  let fixture: ComponentFixture<ListBooksItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListBooksItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListBooksItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
