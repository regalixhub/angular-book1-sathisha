export class Book {
    constructor(
        isbn: Number,
        title: string,
        volume: string,
        author: string,
        price: Number,
        pages: Number,
        type: string,
        description: string) { }
}
