import { BookAppService } from './../book-app.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-books',
  templateUrl: './list-books.component.html',
  styleUrls: ['./list-books.component.css']
})
export class ListBooksComponent implements OnInit {

  public books = [];
  // public index;

  constructor(private bookService: BookAppService,
              private _router: Router) { }

  ngOnInit() {
    this.books = this.bookService.getBooks();
  }

  // edit(index) {
  //   this._router.navigate(['/edit', index]);
  // }

  // delete(i) {
  //   this.bookService.deleteBook(i);
  // }
}

