import { BookAppService } from './book-app.service';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListBooksComponent } from './list-books/list-books.component';
import { BookFormComponent } from './book-form/book-form.component';
import { UpdateBookComponent } from './update-book/update-book.component';
import { BookDetailsComponent } from './book-details/book-details.component';
import { ListBooksItemComponent } from './list-books-item/list-books-item.component';

@NgModule({
  declarations: [
    AppComponent,
    ListBooksComponent,
    BookFormComponent,
    UpdateBookComponent,
    BookDetailsComponent,
    ListBooksItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [BookAppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
