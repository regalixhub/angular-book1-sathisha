import { BookAppService } from './../book-app.service';
import { Component, OnInit } from '@angular/core';
import { Book } from '../book';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';

@Component({
  selector: 'app-update-book',
  templateUrl: './update-book.component.html',
  styleUrls: ['./update-book.component.css']
})
export class UpdateBookComponent implements OnInit {

  public book: Book;
  topics = ['-none-',
            'Technical book',
            'Biography',
            'Novel',
            'Health care',
            'Comic',
            'Others'];

  constructor(private bookService: BookAppService,
              private _route: ActivatedRoute,
              private _router: Router) { }

  ngOnInit() {
    this._route.paramMap.subscribe((params: ParamMap) => {
      const index = parseInt(params.get('index'), 0);
      this.book = this.bookService.getBook(index);
    });

  }

  update(data) {
    this._router.navigate(['/list']);
  }

}
