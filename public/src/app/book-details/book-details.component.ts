import { Component, OnInit } from '@angular/core';
import { Book } from '../book';
import { BookAppService } from '../book-app.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit {

  public book: Book;
  constructor(private bookService: BookAppService,
              private _route: ActivatedRoute,
              private _router: Router) { }

  ngOnInit() {
    console.log('inside before');
    this._route.paramMap.subscribe((params: ParamMap) => {
      const index = parseInt(params.get('index'), 0);
      this.book = this.bookService.getBook(index);
    });
  }

  edit(index) {
    this._router.navigate(['/edit', index]);
    // this._router.navigate([index], {relativeTo: this._route});
  }
  delete(index) {
    this.bookService.deleteBook(index);
  }

}
