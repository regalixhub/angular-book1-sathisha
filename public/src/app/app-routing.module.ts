import { AppComponent } from './../../../my-app/src/app/app.component';
import { BookDetailsComponent } from './book-details/book-details.component';
import { UpdateBookComponent } from './update-book/update-book.component';
import { BookFormComponent } from './book-form/book-form.component';
import { ListBooksComponent } from './list-books/list-books.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path : '', redirectTo: '/list', pathMatch : 'full'},
  {path : 'list', component: ListBooksComponent},
  {path: 'add', component : BookFormComponent},
  {path: 'edit/:index', component : UpdateBookComponent},
  {path: 'showDetails/:index', component: BookDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
