import { BookAppService } from './../book-app.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent implements OnInit {

  topicHasError = true;
  submitted = false;
  topics = ['-none-',
            'Technical book',
            'Biography',
            'Novel',
            'Health care',
            'Comic',
            'Others'];

  constructor(private bookService: BookAppService,
              private _router: Router) { }

  ngOnInit() {
  }

  onClickSubmit(data) {
    console.log(data);
    this.bookService.addBook(data);
    this._router.navigate(['/list']);
    alert('Your book added into the list..!');
  }

}
