import { Injectable } from '@angular/core';
import { Book } from './book';

@Injectable({
  providedIn: 'root'
})
export class BookAppService {

  public book = new Book(null, '', '', '', null, null, '', '');
  public books = //[];
  [{isbn: 1234,
    title: 'book title',
    volume: 'volume 1',
    author: 'author name',
    price: 20,
    pages: 300,
    type: 'Novel',
    description: 'Others'}];

  constructor() {
  }

  getBooks() {
    return this.books;
  }
  addBook(data: Book) {
      this.books.push(data);
      console.log(data);
  }
  deleteBook(i) {
    this.books.splice(i, 1);
  }
  getBook(index: number): Book {
    console.log(this.books);
     return this.books[index];
  }
}
